# Subspace AltDocs

![CI Status](https://ci.codeberg.org/api/badges/Seryoga_Leshii/subspace-altdocs/status.svg "CI Status")

**English** - [Русский][ru]

Subspace AltDocs was created as a place,
where it will be possible to collect a variety of useful information 
for the farmers of the Subspace project.
It doesn't have to be directly related to Subspace -- for example, general system optimizations.

You can access it there:

https://seryoga_leshii.codeberg.page/subspace-altdocs/en/

## Contributing

The project is under active development. 
If you want to contribute to the project, there are 2 main ways:

1) Open an issue at Codeberg repository
2) Sumbit a pull request to Codeberg repository.

If you chose the second option, then you will need to
know how to work with files in the ReStructuredText (ReST) format.
I would recommend to study the following materials:

https://docutils.sourceforge.io/docs/user/rst/quickref.html

https://www.sphinx-doc.org/en/master/usage/index.html

[ru]: README.ru.md
