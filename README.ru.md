# Subspace AltDocs

![CI Status](https://ci.codeberg.org/api/badges/Seryoga_Leshii/subspace-altdocs/status.svg "CI Status")

[English][en] - **Русский**

Subspace AltDocs создавался как место, 
где можно будет собирать разнообразную полезную информацую для фермеров проекта Subspace.
Она необязательно должна быть напрямую связанна с Subspace -- например, общие системные оптмизации. 

Ты можешь получить доступ тут:

https://seryoga_leshii.codeberg.page/subspace-altdocs/ru/

## Внести вклад в проект

Проект находится на стадии активной доработки. 
Если ты хочешь внести свой вклад в проект, то есть 2 основных способа:

1) Создать тему в Codeberg-репозитории
2) Отправить Pull Request в Codeberg-репозиторий.

Если ты выбрал второй вариант, 
то тебе необходимо знать, как работать с файлами в формате ReStructuredText (ReST).
Я бы порекомендовал изучить следующие ресурсы:

https://restructuredtext.ru/

https://sphinx-ru.readthedocs.io/ru/latest/sphinx.html


[en]: README.md
