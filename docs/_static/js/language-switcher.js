var languageSwitchBtns = document.querySelectorAll("language-switcher__button");

if (languageSwitchBtns) {
    const currentFilePath = `${DOCUMENTATION_OPTIONS.pagename}.html`;
    
    Object.values(DOCUMENTATION_OPTIONS.languages).forEach((lang) => {
	const span = document.createElement("span");
	span.textContent = lang;

	const node = document.createElement("a");
	node.setAttribute(
          "class",
          "list-group-item list-group-item-action py-1",
        );
        node.setAttribute("href", `/${lang}/${currentFilePath}`);
        node.appendChild(span);

	node.onclick = node.getAttribute("href");

	document.querySelector(".language-switcher__menu").append(node);
    });
}
