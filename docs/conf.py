# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information


project = 'Subspace AltDocs'
author = 'Seryoga Leshii'
copyright = '2023, ' + author
release = '0.0.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ['sphinx_copybutton', 'sphinx_design']

# master_doc = 'index'
# root_doc = master_doc

source_suffix = { '.rst': 'restructuredtext' }

smartquotes = False

highlight_language = 'console'

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

language = "en"

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'monokai'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output
html_theme = 'pydata_sphinx_theme'

html_static_path = ['_static']

html_css_files = [
    'css/language-switcher.css',
    'css/theme.css',
]

html_title = project

search_localization = {
    "en": "Search...",
    "ru": "Поиск...",
}

html_theme_options = {
    "header_links_before_dropdown": 9999,
    "navbar_center": [],
    "navbar_end": ["theme-switcher", "language-switcher.html", "navbar-icon-links"],
    "navbar_align": "content",
    "show_nav_level": 4,
    "navigation_depth": 4,
    "show_toc_level": 4,
    "collapse_navigation": True,
    "use_edit_page_button": False,
    "secondary_sidebar_items": ["page-toc"],
    "icon_links": [
        {
            "name": "Codeberg",
            "url": "https://codeberg.org/Seryoga_Leshii/subspace-altdocs",
            "icon": "fab fa-git-alt fa-lg",
        },
        {
            "name": "Discord",
            "url": "https://discord.gg/subspace-network",
            "icon": "fab fa-discord fa-lg"
        }
    ],
    'search_bar_text': search_localization[language],
    "footer_items": ["copyright"],
}

html_context = {
    "default_mode": "dark",
    "github_url": "https://codeberg.org",
    "github_user": "Seryoga_Leshii",
    "github_repo": "subspace-altdocs",
    "github_version": "main",
    "doc_path": "docs",
    "languages": {
        "en": "English",
        "ru": "Русский",
    },
}

html_sidebars = {
    "**": ['navbar-nav-uncut.html']
}

html_copy_source = False
html_show_sourcelink = False
html_show_sphinx = False
