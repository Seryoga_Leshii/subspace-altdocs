.. Subspace AltDocs (c) 2023, Seryoga Leshii

   Subspace AltDocs is licensed under a
   Creative Commons Attribution-ShareAlike 4.0 International License.

   You should have received a copy of the license along with this
   work. If not, see <https://creativecommons.org/licenses/by-sa/4.0/>.

.. _introduction:

#################
Subspace AltDocs
#################

Greetings to you. Subspace AltDocs was created as a place,
where you can collect various tips, settings, and manuals useful for Subspace farmers.
So far, almost everything here only applies to most Linux distributions
and possibly some other nix operating systems.
**You perform all actions at your own peril and risk, the author does not bear any responsibility.**
If you need help or have some questions, then you can ask them on the `official Subspace Discord server
<https://discord.gg/subspace-network>`_. If you'd like to suggest any fixes or additions,
then create a thread on `CodeBerg Issues <https://codeberg.org/Seryoga_Leshii/subspace-altdocs/issues>`_.

Know that mindless execution of commands will not lead to good.
Try to understand what you are going to do and why.
For most actions, there should be explanatory comments,
but if after reading them something is still not clear to you, then do not hesitate to ask others.

.. Also at the end of some sections there is a list of commands used in it and explanations for them.

.. toctree::
   :maxdepth: 10

   source/linux-related/index
   source/subspace-related/index



