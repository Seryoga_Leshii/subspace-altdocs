.. Subspace AltDocs (c) 2023, Seryoga Leshii

   Subspace AltDocs is licensed under a
   Creative Commons Attribution-ShareAlike 4.0 International License.

   You should have received a copy of the license along with this
   work. If not, see <https://creativecommons.org/licenses/by-sa/4.0/>.

.. _pam-hardening:
   
****
PAM
****

PAM is a framework for user authentication — it's what you use when you login.
You can make it more secure by requiring strong passwords or enforcing delays upon failed login attempts.

.. _pam-restricting-su:

===============
Restricting su
===============

``su`` lets you switch users from a terminal.
By default, it tries to login as root.
To restrict the use of ``su`` to users within the ``wheel`` group,
edit ``/etc/pam.d/su``::

  sudo -e /etc/pam.d/su

Uncomment this line::

  #auth           required        pam_wheel.so use_uid

If it is missing, then add it yourself.

Do the same actions with file ``/etc/pam.d/su-l``.


.. _pam-hashing-rounds:

====================================
Increasing number of hashing rounds
====================================

You can increase the number of hashing rounds,
thereby increasing the security of your hashed passwords
by forcing an attacker to compute substantially more hashes to crack your password.
By default, shadow uses 5000 rounds, but you can increase this to as many as you want.
Although the more rounds you configure, the slower it will be to login.

Open ``/etc/pam.d/passwd``::

  sudo -e /etc/pam.d/passwd

On a line like this::

  password        required        pam_unix.so sha512 shadow nullok

Add ``rounds=65536`` at the end::

  password        required        pam_unix.so sha512 shadow nullok rounds=65536

Your passwords are not automatically rehashed after applying this setting,
so you need to reset the password with::

  passwd


.. _pam-strong-passwd:

===========================
Enforcing strong passwords
===========================

Strong passwords are very important for system security.
To enforce strong passwords, you can use ``pam_pwquality`` module.
It enforces a configurable policy for passwords.

First, install it:

.. tab-set::

   .. tab-item:: Ubuntu

      ::

	 sudo apt install libpam-pwquality

   .. tab-item:: Arch Linux

      ::

	 sudo pacman -S libpwquality

Then open the file ``/etc/pam.d/passwd``::

  sudo -e /etc/pam.d/passwd

And add this line to it::
	 
  password        required        pam_pwquality.so retry=2 minlen=16 difok=6 dcredit=-3 ucredit=-2 lcredit=-2 ocredit=-3 enforce_for_root

These settings enforce the following rules: at least 16 characters, at least 6 different characters from the old password, at least 3 digits, at least 2 uppercase, at least 2 lowercase and at least 3 other characters.


.. _pam-login-delays:

=======================
Enforcing login delays 
=======================

To add a delay of at least 4 seconds between failed login attempts to deter bruteforcing attempts,
edit ``/etc/pam.d/system-login``::

  sudo -e /etc/pam.d/system-login

And add the following line::

  auth       optional   pam_faildelay.so delay=4000000

"4000000" being 4 seconds in microseconds.

