.. Subspace AltDocs (c) 2023, Seryoga Leshii

   Subspace AltDocs is licensed under a
   Creative Commons Attribution-ShareAlike 4.0 International License.

   You should have received a copy of the license along with this
   work. If not, see <https://creativecommons.org/licenses/by-sa/4.0/>.

.. _filesystem-hardening:

************************
Filesystem hardening
************************

This section is about securing filesystems, mount points, file permissions and the similar.

.. _mount-points-hardening:

=============
Mount points
=============

File systems should be separated into various partitions to gain fine-grained control over their permissions.
Different mount options can be added to restrict what can be done:

* ``nodev`` -- Disallow devices
* ``nosuid`` -- Disallow setuid and setgid bits, which allow binary files to be executed on behalf of their owner.
* ``noexec`` -- Disallow executing any binaries (does not apply to scripts)

More or less universal parameters are proposed below.
Open the file ``/etc/fstab``::

  sudo -e /etc/fstab

And at the end add the following lines::
   
   # home
   /home                   /home           none            bind,nodev  0 2

   # boot
   /boot                   /boot           none            bind,nosuid,nodev,noexec  0 2

   # var
   /var                    /var            none            bind,nosuid  0 2

The changes will take effect after a reboot.


.. _proc-hardening:

===============
Proc hardening
===============

``/proc`` is a pseudo-filesystem that contains information about all processes currently running on the system.
By default, this is accessible to all users,
which can allow an attacker to spy on other processes.
To permit users to only see their own processes and not those of other users,
you must mount ``/proc`` with the specific mount options.

Open the file ``/etc/fstab``::

  sudo -e /etc/fstab

Then add the following lines::

  # proc
  proc                    /proc           proc            nosuid,nodev,noexec,hidepid=2,gid=proc  0 0

``gid=proc`` exempts the ``proc`` group from this feature so you can whitelist specific users or processes.

``systemd-logind`` still needs to see other users processes,
so for user sessions to work correctly on a systemd system (Ubuntu and Arch Linux, as example).
To allow him to see them, first create a configuration directory::

  mkdir -p /etc/systemd/system/systemd-logind.service.d/

Then open the config file::

  sudo -e /etc/systemd/system/systemd-logind.service.d/hidepid.conf

And add there the following::

  [Service]
  SupplementaryGroups=proc

Changes will take effect after reboot.


.. _file-permissinons:

=================
File permissions
=================

By default, the permissions of files are quite permissive.
Therefore, you should restrict files with improper permissions by yourself.

In many Linux distributions, home directories are world-readable by default.
It is worth restricting permissions on them::

  sudo chmod 700 /home/*
  
A few more examples are
``/boot``, ``/usr/src`` and ``/{,usr/}lib/modules`` — these contain
the kernel image, System.map and various other files,
all of which can leak sensitive information about the kernel.
To restrict access to these, execute:

.. tab-set::

   .. tab-item:: Ubuntu

      ::

	 sudo dpkg-statoverride --update --add root root 700 /boot

	 sudo dpkg-statoverride --update --add root root 700 /usr/src

	 sudo dpkg-statoverride --update --add root root 700 /lib/modules
	 
	 sudo dpkg-statoverride --update --add root root 700 /usr/lib/modules


   .. tab-item:: Others

      ::

	 sudo chmod 700 /boot /usr/src /lib/modules /usr/lib/modules

It is also worth restricting the default permissions for newly created files.
The ``umask`` utility is used for this. The default umask is 0022, which is not very secure,
as this gives read access to every user on the system for newly created files.
To make changes for all users on the system, edit the ``/etc/profile`` file::

  sudo -e /etc/profile

Then try to find an existing umask line. For example::

  umask 022

Change the value to something more restrictive::

  umask 077

If this line did not originally exist,
then add it closer to the beginning of the file.

