.. Subspace AltDocs (c) 2023, Seryoga Leshii

   Subspace AltDocs is licensed under a
   Creative Commons Attribution-ShareAlike 4.0 International License.

   You should have received a copy of the license along with this
   work. If not, see <https://creativecommons.org/licenses/by-sa/4.0/>.

.. _identifiers:

************
Identifiers
************

For privacy, it is best to minimise the amount of information that can be traced back to you.

.. _locale-hardening:

=======
Locale
=======

Using locales other than the default ``en_US.UTF-8`` can help deanonymize the server owner.
You can check the currently set locale with the command::

  locale

You should see something like this::

  LANG=en_US.UTF-8
  LC_CTYPE="en_US.UTF-8"
  LC_NUMERIC="en_US.UTF-8"
  LC_TIME="en_US.UTF-8"
  LC_COLLATE="en_US.UTF-8"
  LC_MONETARY="en_US.UTF-8"
  LC_MESSAGES="en_US.UTF-8"
  LC_PAPER="en_US.UTF-8"
  LC_NAME="en_US.UTF-8"
  LC_ADDRESS="en_US.UTF-8"
  LC_TELEPHONE="en_US.UTF-8"
  LC_MEASUREMENT="en_US.UTF-8"
  LC_IDENTIFICATION="en_US.UTF-8"
  LC_ALL=

If your ``LANG`` value is different, then you should change it:

.. tab-set::

   .. tab-item:: Ubuntu

      ::

	 sudo update-locale LANG=en_US.UTF-8

   .. tab-item:: Arch Linux

      ::

	 sudo localectl set-locale LANG=en_US.UTF-8


.. _username-hardening:

=========
Username
=========

Usernames can deanonymize the server owner, so it's worth using more generic ones.
For example, ``user``.  Changing the username can be a bit tricky, so it's omitted here for now.


.. _hostname-hardening:

=========
Hostname
=========

Similar to a username,
hostname can deanonymize the server owner, so it's worth using more generic ones.
For example, ``host``::

  sudo hostnamectl set-hostname host

Changes will take effect after reboot.

Before rebooting,
replace all occurrences of the old hostname with the new one in the ``/etc/hosts`` file.


.. _machine-id-hardening:

===========
Machine ID
===========

A unique Machine ID is stored in ``/etc/machine-id`` on systemd systems::

  sudo -e /etc/machine-id
  
These should be edited to something generic, such as the Whonix ID::

  b08dfa6083e7567a1921a715000001fb


.. _time-sync-hardening:

=====================
Time synchronisation
=====================

Time synchronisation is vital for anonymity and security.
A wrong system clock can expose you to clock skew fingerprinting attacks
or can be used to feed you outdated HTTPS certificates,
bypassing certficate expiry or revocation.

The most popular time synchronisation method, NTP,
is insecure, as it is unencrypted and unauthenticated,
allowing an attacker to trivially intercept and modify requests.
NTP also leaks your local system time in NTP timestamp format,
which can be used for clock skew fingerprinting, as briefly mentioned before.

With NTPsec a 'secure' replacement is possible:

.. tab-set::

   .. tab-item:: Ubuntu

      Install NTPsec itself::

	sudo apt install ntpsec

      Disable systemd-timesyncd::

	sudo systemctl disable --now systemd-timesyncd

      Enable NTP daemon::

	sudo systemctl enable --now ntpd

	
   .. tab-item:: Arch Linux

      Create ``~/src`` directory::

	mkdir -p ~/src

      Go into it::

	cd ~/src

      Clone the repository from the AUR::

	git clone https://aur.archlinux.org/ntpsec.git

      Go into directory::

	 cd ntpsec

      Build and Install NTPsec::

	 makepkg -sricfC

      Enable NTP daemon::

	sudo systemctl enable --now ntpd

   
