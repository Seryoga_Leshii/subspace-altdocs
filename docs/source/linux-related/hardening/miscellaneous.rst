.. Subspace AltDocs (c) 2023, Seryoga Leshii

   Subspace AltDocs is licensed under a
   Creative Commons Attribution-ShareAlike 4.0 International License.

   You should have received a copy of the license along with this
   work. If not, see <https://creativecommons.org/licenses/by-sa/4.0/>.

.. _miscellaneous-hardening:

************************
Miscellaneous hardening
************************

This section contains various techniques for securing the system that do not fit into the existing categories.

.. _creating-admin-user:

====================
Creating admin user
====================

Running as root can be very dangerous.
In this guide, a locking of the root account is performed,
so that it is almost impossible to log into it from the outside.

If you do not have such a user, then you should create it.
But before that, check out :ref:`this <username-hardening>`.

An example of a command that creates a user named ``user``::

   useradd -m user -G wheel

Set a strong password::

  passwd user

Now you need to make it an administrator.
To do this, run the command::

  EDITOR=nano visudo

Near the end of the file, find the following line::

  # %wheel ALL=(ALL:ALL) ALL

Uncomment it::

  %wheel ALL=(ALL:ALL) ALL

And save the changes.

.. _ssh-hardening:

====
SSH
====

SSH is one of the most common methods of accessing remote servers.
SSH is also one of the most common reasons behind compromised Linux servers.
Although by design it is pretty secure, its default settings are not.

SSHD configuration is stored in ``/etc/ssh/sshd_config``.
Open it on your server, as further instructions assume that you have it open.


--------------------
Login by public key
--------------------

The ability to log into the server using the user's password
makes it possible for an attacker to try to guess it (brute force).
Therefore you should disable it and use only public keys.

If you don't have a key, then you must generate one.
The following are ways to generate and use keys for different systems:

.. tab-set::

   .. tab-item:: Linux

      ::

	 ssh-keygen -b 4096 -t rsa

      After entering this command,
      you will be asked for a place to store the keys,
      and it is better to leave it by default.

      You are also asked for a password to encrypt the key.
      I advise you to set a fairly strong one.

      After you need to add your public key to the list of authorized on your server.
      This can be done with the command::

	ssh-copy-id user@host

      Where ``user`` is the target user on the server and ``host`` is its address.

      Alternatively, you can manually paste the contents of your public key file
      (``~/.ssh/id_rsa.pub``, for example)
      into the ``~/.ssh/authorized_keys`` file on the target server.

      Try to enter the server.
      If you did everything right,
      then you should see something like::

	 Enter passphrase for key '/home/user/.ssh/id_rsa':

      
   .. tab-item:: MobaXTerm

      TODO

After you have generated a key for yourself,
and also learned how to use it,
it is worth enforcing logging in using a public key.

Set values for the following options::

  PasswordAuthentication no

  KbdInteractiveAuthentication no

  
----------------
Change the port
----------------

Changing the default SSHD port,
along with protection from port scanning (read about it here),
could improve security.
It complicates the task for an attacker, since he will not know which port to attack.

Set the following option to something else::

  Port 22

For example::

  Port 25567

You must also specify the new port explicitly when logging in:

.. tab-set::

   .. tab-item:: Linux

      Add ``-p <port>`` to the command,
      where ``<port>`` is the SSHD port on the server.
      For example, it was::

	ssh user@host

      It is::

	ssh -p 25567 user@host


   .. tab-item:: MobaXTerm

      TODO


-------------------
Disable root login
-------------------

Using server as root itself should be forbidden.
It is risky and leaves no audit trail.

To disable root login set the following option::

  PermitRootLogin no


-----------------------
Disable X11 forwarding
-----------------------

The X11 or the X display server is the basic framework for a graphical environment.
The X11 forwarding allows you to use a GUI application via SSH.

The X11 protocol is not security oriented.
If you don’t need it, you should disable the X11 forwarding in SSH.

To do this set the following option::

  X11Forwarding no

-----------------------------
Restriction of allowed users
-----------------------------

It makes sense to allow only certain users to login via SSH.
This could be achieved using the following option::

  AllowUsers

It will most likely not be in the configuration file by default, so add it yourself.
If you are using the server exclusively by yourself,
then the following value will do::

  AllowUsers user

------------------------
Disable port forwarding
------------------------

This functionality may cause security holes. 
Therefore, if not used, should be disabled.

To do this set the following options::

  AllowAgentForwarding no
  
  AllowTcpForwarding no

For the changes to take effect,
you must restart the SSHD::

  sudo systemctl restart sshd

.. _securetty-hardening:

===============
/etc/securetty
===============

The file, ``/etc/securetty`` specifies where you are allowed to login as root from.
This file should be kept empty so that nobody can do so from a terminal.

.. _locking-root-account:

=========================
Locking the root account
=========================

To lock the root account to prevent anyone from ever logging in as root, execute::

  passwd -l root

Make sure that you have an alternative method of gaining root
(such as booting from a live USB and chrooting into the filesystem)
before doing this so you do not inadvertently lock yourself out of the system.

.. _core-dumps-hardening:

===========
Core dumps
===========

Core dumps contain the recorded memory of a program at a specific time,
usually when that program has crashed.
These can contain sensitive information,
such as passwords and encryption keys, so these must be disabled.

Edit ``/etc/security/limits.conf`` and add::

  * hard core 0
  root soft core 0


.. _microcode:

==========
Microcode
==========

Microcode updates are essential to fix critical CPU vulnerabilities.

.. tab-set::

   .. tab-item:: Ubuntu

      Install microcode for your CPU vendor:

      .. tab-set::

	 .. tab-item:: AMD

	    ::

	       sudo apt install amd64-microcode

	 .. tab-item:: Intel

	    ::

	       sudo apt install intel-microcode

      And update your GRUB configuration::

	sudo update-grub

	
   .. tab-item:: Arch Linux

      Install microcode for your CPU vendor:

      .. tab-set::

	 .. tab-item:: AMD

	    ::

	       sudo pacman -S amd-ucode

	 .. tab-item:: Intel

	    ::

	       sudo pacman -S intel-ucode

      And update you GRUB configuration::

	sudo grub-mkconfig -o /boot/grub/grub.cfg


.. _editing-hardening:

======================
Editing files as root
======================

It is unrecommended to run ordinary text editors as root.
Most text editors can do much more than simply edit text files,
and this can be exploited.
For example, open ``vi`` as root and enter ``:sh``.
You now have a root shell with access to your entire system,
which an attacker can easily exploit.

A solution to this is using ``sudo -e``.
This copies the file to a temporary location,
opens the text editor as an ordinary user,
edits the temporary file and overwrites the original file as root.
This way, the actual editor doesn't run as root.
To use ``sudo -e``, execute::

  sudo -e <path>

Where ``<path>`` is the path to the target file.

By default, it uses ``vi``,
but the default editor can be switched via the ``EDITOR``
or ``SUDO_EDITOR`` environment variables.
If you are not familiar with other editors,
I recommend using ``nano``.
To do this, add the following line to the ``/etc/environment`` file::

  EDITOR=nano

Changes will take effect with the start of a new session.

  


	       

