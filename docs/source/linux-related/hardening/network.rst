.. Subspace AltDocs (c) 2023, Seryoga Leshii

   Subspace AltDocs is licensed under a
   Creative Commons Attribution-ShareAlike 4.0 International License.

   You should have received a copy of the license along with this
   work. If not, see <https://creativecommons.org/licenses/by-sa/4.0/>.

********
Network
********

This section is dedicated to network securing. For example, via firewall.


.. _firewall:

=========
Firewall
=========

Firewalls can control incoming and outgoing network traffic
and can be used to block or allow certain types of traffic.
You should always block all incoming traffic unless you have a specific reason not to.
It is recommended to set up a strict iptables or nftables firewall.
Firewalls must be fine-tuned for your system,
and there is not one ruleset that can fit all of them.
It is recommended to get familiar with creating firewall rules.

------------
Basic setup
------------

Below is a proposed firewall configuration that you can use on your server with nodes.
UFW is used as a tool for managing firewall rules.

.. tab-set::

   .. tab-item:: Ubuntu

      Install::

	sudo apt install ufw

      And enable::

	sudo systemctl enable --now ufw
      

   .. tab-item:: Arch Linux

      Install::

	sudo pacman -S ufw

      And enable::

	sudo systemctl enable --now ufw

	
Now open the ``/etc/ufw/before.rules`` file
and before the COMMIT line add the following::

  # prevent masked attacks
  -A ufw-before-input -p icmp --icmp-type 13 -j DROP
  -A ufw-before-input -p icmp --icmp-type 17 -j DROP
  -A ufw-before-input -p icmp --icmp-type 14 -j DROP
  -A ufw-before-input -p icmp -m limit --limit 1/second -j ACCEPT

  # drop spoofing attacks
  -A ufw-before-input -s 169.254.0.0/16 -j DROP
  -A ufw-before-input -s 127.0.0.0/8 -j DROP
  -A ufw-before-input -s 224.0.0.0/4 -j DROP
  -A ufw-before-input -d 224.0.0.0/4 -j DROP
  -A ufw-before-input -s 240.0.0.0/5 -j DROP
  -A ufw-before-input -d 240.0.0.0/5 -j DROP
  -A ufw-before-input -s 0.0.0.0/8 -j DROP
  -A ufw-before-input -d 0.0.0.0/8 -j DROP
  -A ufw-before-input -d 239.255.255.0/24 -j DROP
  -A ufw-before-input -d 255.255.255.255 -j DROP

  # drop packets with excessive RST to avoid masked attacks
  -A ufw-before-input -p tcp -m tcp --tcp-flags RST RST -m limit --limit 2/second --limit-burst 2 -j ACCEPT

  # any IP that performs a PortScan will be blocked for 24 hours
  -A ufw-before-input   -m recent --name portscan --rcheck --seconds 86400 -j DROP
  -A ufw-before-forward -m recent --name portscan --rcheck --seconds 86400 -j DROP

  # after 24 hours remove IP from block list
  -A ufw-before-input   -m recent --name portscan --remove
  -A ufw-before-forward -m recent --name portscan --remove

  # log port scan attemts
  -A ufw-before-input   -p tcp -m tcp --dport 139 -m recent --name portscan --set -j LOG --log-prefix "[UFW PORTSCAN]"
  -A ufw-before-input   -p tcp -m tcp --dport 139 -m recent --name portscan --set -j DROP

  -A ufw-before-forward -p tcp -m tcp --dport 139 -m recent --name portscan --set -j LOG --log-prefix "[UFW PORTSCAN]"
  -A ufw-before-forward -p tcp -m tcp --dport 139 -m recent --name portscan --set -j DROP

  # allow ping
  -A ufw-before-input -p icmp --icmp-type 0 -j ACCEPT

So far, the firewall is inactive.
Before enabling it, be sure to allow traffic on your SSH port,
otherwise you will not be able to access your server.
The following section shows examples of using UFW.


---------------------------
Basics of working with UFW
---------------------------

UFW is very easy to use. Below are examples of how to perform the most common actions using UFW.

Allow any traffic on port 80::

  sudo ufw allow 80

Allow any TCP traffic on port 80::

  sudo ufw allow 80/tcp

Delete the above rule::

  sudo ufw delete allow 80/tcp

Allow any TCP traffic on multiple ports::

  sudo ufw allow 80,443,999/tcp

Allow any UDP traffic on port range.
Note, that you must explicitly specify the protocol::

  sudo ufw allow 80-123/udp

Allow forwarding TCP traffic to any destination with port 80::

  sudo ufw route allow proto tcp from any to any port 80

Delete this rule::

  sudo ufw route delete allow proto tcp from any to any port 80

For some widely used protocols, there are already ready-made rules.
For example, allow TCP traffic for http (port 80)::

  sudo ufw allow http

You should definitely allow http, https and ntp traffic,
and also for your SSH port.

Enable filewall::

  sudo ufw enable

Disable firewall::

  sudo ufw disable

Reload firewall rules::

  sudo ufw reload


-------
Docker
-------

UFW may not work with Docker by default.
Therefore, if you use it, it is worth performing some manipulations.
Open the file ``/etc/ufw/after.rules`` and at the very end of the file add the following::

   # BEGIN UFW AND DOCKER
   *filter
   :ufw-user-forward - [0:0]
   :ufw-docker-logging-deny - [0:0]
   :DOCKER-USER - [0:0]
   -A DOCKER-USER -j ufw-user-forward
   
   -A DOCKER-USER -j RETURN -s 10.0.0.0/8
   -A DOCKER-USER -j RETURN -s 172.16.0.0/16
   -A DOCKER-USER -j RETURN -s 192.168.0.0/16
   
   -A DOCKER-USER -p udp -m udp --sport 53 --dport 1024:65535 -j RETURN
   
   -A DOCKER-USER -j ufw-docker-logging-deny -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -d 192.168.0.0/16
   -A DOCKER-USER -j ufw-docker-logging-deny -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -d 10.0.0.0/8
   -A DOCKER-USER -j ufw-docker-logging-deny -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN -d 172.16.0.0/12
   -A DOCKER-USER -j ufw-docker-logging-deny -p udp -m udp --dport 0:32767 -d 192.168.0.0/16
   -A DOCKER-USER -j ufw-docker-logging-deny -p udp -m udp --dport 0:32767 -d 10.0.0.0/8
   -A DOCKER-USER -j ufw-docker-logging-deny -p udp -m udp --dport 0:32767 -d 172.16.0.0/12
   
   -A DOCKER-USER -j RETURN
   
   -A ufw-docker-logging-deny -m limit --limit 3/min --limit-burst 10 -j LOG --log-prefix "[UFW DOCKER BLOCK] "
   -A ufw-docker-logging-deny -j DROP
   
   COMMIT
   # END UFW AND DOCKER

You can read more about this `here <https://github.com/chaifeng/ufw-docker>`_.

If you want to allow public networks to access the services provided by the Docker container,
for example, the service port of a container is ``80``.
Run the following command to allow the public networks to access this service::

  sudo ufw route allow proto tcp from any to any port 80


.. _honeypot:

=========
Honeypot
=========

Honeypot is a trap for a potential intruder.
Usually, protection actions are automatically taken,
and with the help of it, you can learn about the attack in a timely manner.

Below is the installation and configuration of Artillery as a honeypot.

Create a ``~/src`` directory if you haven't already done so::

  mkdir -p ~/src

Go into it::

  cd ~/src

Download Artillery source::

  git clone https://github.com/BinaryDefense/artillery

Go to directory with source::

  cd artillery

Install it by running the command::

  sudo ./setup.py

During installation, you may observe various kinds of errors.
Don't worry, this is normal.

Answer with ``y`` here::

  Do you want to install Artillery and have it automatically run when you restart [y/n]:

Then install the service file for Artillery::

  sudo cp src/artillery_service /etc/systemd/system/artillery.service

Now you need to set it up.
Open the file ``/var/artillery/config``
and set the appropriate parameters to the following values::

  AUTO_UPDATE="OFF"

  BIND_INTERFACE="0.0.0.0"

Replace ``0.0.0.0`` with your external IP address.

Now you need to allow traffic on the ports you need for Artillery.
Here's how to do it with UFW::

  sudo ufw allow 21,22,53,110,1337,1433,1723,5800,5900,8080,10000,16993,44443/tcp

  sudo ufw allow 53,3478,5060,5061/udp

Now you can enable Artillery::

  sudo systemctl enable --now artillery


  
.. _port-scan-protection:

====================
Portscan protection
====================

A port scanning is a probing a server or host for open ports.
Port scanning is one of the most popular techniques hackers use
to discover vulnerabilities and exploit services to break into systems.

:abbr:`PSAD (Port Scan Attack Detection)` is used to block post scanning attemts on the server.
Psad tool keeps on monitoring firewall logs to determine port scan or any other attack occurred.
If some successful attack on the server happens psad also takes action to detect the threat.

Installation:

.. tab-set::

   .. tab-item:: Ubuntu

      ::

	 sudo apt install psad

   .. tab-item:: Arch Linux

     Create ``~/src`` directory::

	mkdir -p ~/src

      Go into it::

	cd ~/src

      Clone the repository of one of dependencies from AUR::

	git clone https://aur.archlinux.org/perl-iptables-parse.git

      Go into directory::

	cd perl-iptables-parse

      Build and install it::

	makepkg -sricfC

      Go back::

	cd ~/src

      Clone the repository of one of dependencies from AUR::

        git clone https://aur.archlinux.org/perl-iptables-chainmgr.git

      Go into directory::

        cd perl-iptables-chainmgr

      Build and install it::

        makepkg -sricfC

      Go back::

        cd ~/src

      Clone the repository from the AUR::

	git clone https://aur.archlinux.org/psad.git

      Go into directory::

	 cd psad

      Build and install PSAD::

	 makepkg -sricfC


Now it's time to configure PSAD.
Open the ``/etc/psad/psad.conf`` file.
Find the following lines and set the corresponding values::

  HOSTNAME                    <hostname>;
  ALERTING_METHODS            noemail;

Replace ``<hostname>`` with your hostname.
Setting the ``ALERTING_METHODS`` parameter to ``noemail`` will disable email notifications.
If you have a mail client configured on the server, then notifications from PSAD can clutter up your mailbox,
so it's better to turn them off.

Now you can enable PSAD daemon::

  sudo systemctl enable --now psad

Note that PSAD is only effective with verbose logging from the firewall.
When using UFW, run the command::

  sudo ufw logging medium
