.. Subspace AltDocs (c) 2023, Seryoga Leshii

   Subspace AltDocs is licensed under a
   Creative Commons Attribution-ShareAlike 4.0 International License.

   You should have received a copy of the license along with this
   work. If not, see <https://creativecommons.org/licenses/by-sa/4.0/>.

.. _linux-related:

##############
Linux-related
##############

This section is dedicated to useful for Subspace farmers Linux-related optimizations, tips, hardening and so on.
I hope you have already read the introduction, but if not, please read it :ref:`there <introduction>`.

.. toctree::
   :maxdepth: 2

   optimization/index
   hardening/index
   miscellaneous




