.. Subspace AltDocs (c) 2023, Seryoga Leshii

   Subspace AltDocs is licensed under a
   Creative Commons Attribution-ShareAlike 4.0 International License.

   You should have received a copy of the license along with this
   work. If not, see <https://creativecommons.org/licenses/by-sa/4.0/>.


.. _miscellaneous:

**************
Miscellaneous
**************

This section is about things that can't be classified as either optimizations or hardenings.


.. _mail-client-setup:

==================
Mail client setup
==================

An email client is a very useful tool for notifying you of problems on the server.
So setting it up can help detect serious problems in time.

This guide shows you how to setup postfix as your send-only mail client.

Installation:

.. tab-set::

   .. tab-item:: Ubuntu

      ::

	 sudo apt install s-nail postfix

   .. tab-item:: Arch Linux

      ::

	 sudo pacman -S s-nail postfix
	 

To configure postfix, run the following commands::

  sudo postconf -e "inet_interfaces = localhost"

  sudo postconf -e "mydestination = localhost.$mydomain, localhost, $myhostname"

Now you can enable postfix::

  sudo systemctl enable --now postfix
  
For convenience, you can setup aliases for mail.
First, you need to find out in which file they are stored on your system.
To do this, run the command::

  postconf alias_maps

Here is an example output::

  alias_maps = hash:/etc/postfix/aliases

In this exapmle,
output shows that mail aliases are stored in ``/etc/postfix/aliases`` file.
Open it and find the line::

  # root:

Uncomment it and add your email address.
For example::

  root: mymail@mydomain.com

For the changes to take effect, enter the command::

  sudo newaliases


