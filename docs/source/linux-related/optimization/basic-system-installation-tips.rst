.. Subspace AltDocs (c) 2023, Seryoga Leshii

   Subspace AltDocs is licensed under a
   Creative Commons Attribution-ShareAlike 4.0 International License.

   You should have received a copy of the license along with this
   work. If not, see <https://creativecommons.org/licenses/by-sa/4.0/>.
   
.. _basic-installation-tips:

*******************************
Basic system installation tips
*******************************

This section is only useful if you are installing the system on your server from scratch.
If you are using servers with pre-installed systems, you can safely skip this section and move on.

.. index:: drives, hdd, ssd
.. _partitioning:

==========================
Proper drive partitioning
==========================

Proper disk layout is very important for both hard drives and SSDs.
The information for both types of drives is described below.

.. caution:: Operations with partition table can result in data loss.

.. index:: hdd, drives
.. _hdd-partitioning:

-----------------
HDD partitioning
-----------------

Incorrect partitioning on already slow hard drives can significantly degrade performance.
However, in modern realities, this is usually not a problem,
since the most popular partitioning programs warn you if you go beyond the logical and/or physical block.
The exception is drives with a physical block size of 4096 bytes, and a logical block size of 512 bytes.
Hard drives capable of operating only in this mode are rare today, but can still be found.
In this case, it is enough to make sure that the partition sizes are a multiple of 4096 bytes.

To determine such a drive, you can use the **fdisk** utility::

  sudo fdisk -l

You should see something like this::

  Disk /dev/sda: 3.5 TiB, 3840766820352 bytes, 7501497696 sectors
  Disk model: SOMEDRIVE
  Units: sectors of 1 * 512 = 512 bytes
  Sector size (logical/physical): 512 bytes / 4096 bytes
  I/O size (minimum/optimal): 4096 bytes / 4096 bytes

As you can see, this drive does not have a partition table.
Here is an example of a properly partitioned hard drive::

  Disk /dev/sda: 3.5 TiB, 3840766820352 bytes, 7501497696 sectors
  Disk model: SOMEDRIVE
  Units: sectors of 1 * 512 = 512 bytes
  Sector size (logical/physical): 512 bytes / 4096 bytes
  I/O size (minimum/optimal): 4096 bytes / 4096 bytes
  Disklabel type: gpt
  Disk identifier: A73F05A1-310E-40E4-8695-6CECB22593B0

  Device      Start        End    Sectors  Size Type
  /dev/sda1    2048     264191     262144  128M EFI System
  /dev/sda2  264192     266239       2048    1M BIOS boot
  /dev/sda3  266240 3750748814 3750482575  3.5T Linux filesystem

Checking the partition size for a multiple of 4096 is easy.
To do this, multiply the size in sectors by 512 and divide by 4096.
As an example, checking the second partition::

   2048 * 512 = 1048576 bytes
   1048576 / 4096 = 256
   or  
   1048576 % 4096 = 0

   
.. index:: ssd, drives
.. _ssd-partitioning:

-----------------
SSD partitioning
-----------------

Incorrect layout of more or less modern SSDs usually does not lead to a significant drop in performance,
especially for NVMe SSDs. However, this does not mean that it should be neglected.
Proper partitioning of a solid state drive can significally extend its service life.
Check out the :ref:`HDD markup <hdd-partitioning>` if you haven't already,
as some of the information there can be applied to SSDs.



.. index:: filesystem, drives
.. _filesystem-selection:

=====================
Filesystem selection
=====================

