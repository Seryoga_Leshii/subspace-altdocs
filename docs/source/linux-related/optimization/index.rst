.. Subspace AltDocs (c) 2023, Seryoga Leshii

   Subspace AltDocs is licensed under a
   Creative Commons Attribution-ShareAlike 4.0 International License.

   You should have received a copy of the license along with this
   work. If not, see <https://creativecommons.org/licenses/by-sa/4.0/>.

.. _linux-optimization:

Linux Optimization
###################

This section is about optimizing the Linux operating system.
The information provided here can be used on most Linux distributions.

.. toctree::
   :maxdepth: 3

   basic-system-installation-tips
   kernel




