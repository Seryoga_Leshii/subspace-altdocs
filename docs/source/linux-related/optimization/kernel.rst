.. Subspace AltDocs (c) 2023, Seryoga Leshii

   Subspace AltDocs is licensed under a
   Creative Commons Attribution-ShareAlike 4.0 International License.

   You should have received a copy of the license along with this
   work. If not, see <https://creativecommons.org/licenses/by-sa/4.0/>.

.. _kernel-optimization:

*******
Kernel
*******

This section is about Linux kernel optimizations.


.. _kernel-options-optimization:
	
===============
Kernel options
===============

The sysctl utility is used to change kernel parameters,
allowing you to change some parameters to achieve optimal performance.
To make temporary changes, you can run::

  sudo sysctl -w <parameter>=<value>

Where ``<parameter>`` -- is the target parameter and
``<value>`` -- is the desired value.

To make changes on a permanent basis,
they need to be added to the ``/etc/sysctl.conf`` file::

  sudo -e /etc/sysctl.conf

The following are the recommended sysctl settings that you should change.


.. _sysctl-sched-optimization:

----------
Scheduler
----------

::

   kernel.sched_autogroup_enabled = 0

When enabled, this setting groups tasks by TTY,
to improve perceived responsiveness on an interactive system.
On a server with a long running forking daemon,
this will tend to keep child processes from migrating away as soon as they should. 


.. _sysctl-fs-optimization:

-----------
Filesystem
-----------

::

   fs.file-max = 100000

When you're serving a lot of traffic
it is usually the case that the traffic you're serving is coming from a large number of local files.
The kernel has built-in limits on the number of files that a process can open,
and raising these limits, at a cost of some system memory, is usually a sane thing to attempt.

::

   vm.min_free_kbytes = 2621440

Specifies the amount of memory in KiB to be kept free from buffering.
This can improve performance when almost all available RAM space is taken up by the cache.
The recommended value is 1-3% of the total amount of RAM.


.. _sysctl-network-optimization:

--------
Network
--------

::

   net.ipv4.tcp_tw_reuse = 1

This allows reuse of s
ockets in ``TIME_WAIT`` state for new connections,
but only when it is safe from the network stack’s perspective.

::

   net.core.somaxconn = 1024

This one increases the maximum number of connections to 1024 (default is 128).

.. tab-set::

   .. tab-item:: ~1GbE

      ::
		 
	 net.core.rmem_max = 16777216
	 net.core.wmem_max = 16777216
	 net.core.rmem_default = 262140
	 net.core.wmem_default = 262140
	 net.ipv4.tcp_rmem = 65535 262140 16777216
	 net.ipv4.tcp_wmem = 65535 262140 16777216
	 net.ipv4.tcp_window_scaling = 1

   .. tab-item:: ~10GbE

      ::

	 net.core.rmem_max = 1073725440
	 net.core.wmem_max = 1073725440
	 net.core.rmem_default = 2621400
	 net.core.wmem_default = 2621400
	 net.ipv4.tcp_rmem = 65535 2621400 1073725440
	 net.ipv4.tcp_wmem = 65535 2621400 1073725440
	 net.ipv4.tcp_window_scaling = 1
	 
The TCP Window Scale option allows window size larger than 64KiB.
This can significantly increase throughput, especially at high network latency.
Note that these settings may not perform well on slow networks (~100MbE).

::

   net.core.default_qdisc = fq
   net.ipv4.tcp_congestion_control = bbr

TCP congestion control and avoidance algorithms (CCAs)
are an important connection tuning consideration,
especially with high bandwidth/high latency broadband networks.
These values are, in general, optimal and proven for most cases.


.. _kernel-build:

====================
Building the kernel
====================

This section is about optimized LTS kernel build.

.. tab-set::

   .. tab-item:: Ubuntu

      TODO
      

   .. tab-item:: Arch Linux

      Install asp utility::

	sudo pacman -S asp

      Create ``~/src`` directory::

	mkdir -p ~/src

      Go into it::

	cd src

      Download LTS kernel PKGBUILD::

	asp chekout linux-lts

      And go into directory::

	cd linux-lts/trunk

      Now you need to make some changes to the ``PKGBUILD`` file.

      Find the ``source`` section. It should look something like this::

	source=(
	  https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/${_srcname}.tar.{xz,sign}
	  config         # the main kernel config file
	  0001-ZEN-Add-sysctl-and-CONFIG-to-disallow-unprivileged-C.patch
	  0002-PCI-Add-more-NVIDIA-controllers-to-the-MSI-masking-q.patch
	  0003-iommu-intel-do-deep-dma-unmapping-to-avoid-kernel-fl.patch
	  0004-Bluetooth-btintel-Fix-bdaddress-comparison-with-garb.patch
	  0005-lg-laptop-Recognize-more-models.patch
	)

      Add a link to the patch there so that it looks like this::

	source=(
	  https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/${_srcname}.tar.{xz,sign}
	  config         # the main kernel config file
	  0001-ZEN-Add-sysctl-and-CONFIG-to-disallow-unprivileged-C.patch
	  0002-PCI-Add-more-NVIDIA-controllers-to-the-MSI-masking-q.patch
	  0003-iommu-intel-do-deep-dma-unmapping-to-avoid-kernel-fl.patch
	  0004-Bluetooth-btintel-Fix-bdaddress-comparison-with-garb.patch
	  0005-lg-laptop-Recognize-more-models.patch
	  https://codeberg.org/Seryoga_Leshii/subspace-altdocs/raw/branch/main/static/cfs-server.patch
	)

      After find the ``sha256sum`` section. It should look something like this::

	sha256sums=('3b321a6466d2021f60ed8d4e33bba21db2f23efc2ddd2d9fb775393d9afdfd4d'
            'SKIP'
            'a509731dffd6942c5b31f92b245cdc543965cfe57aeda0854a470c38e608a4ce'
            '3b5cfc9ca9cf778ea2c4b619b933cda26519969df2d764b5a687f63cf59974cd'
            'c175fbb141c3cec013c799f694d88310375ac5456042f6a4a1adc7667836d786'
            '8357f000b2b622e73dcfd41c2bad42b5e99fffe8f7ee64f774aa771f86cef43c'
            '5c1ee81fdd5818442af6081de987f9c1a9ce3c8d183566b3dfc19a8433aa3dde'
            '067e8995fcd6f6ed25e0253e9374c0e179a000c154da3e59ce62634945ac5be9')

      Add sha256sum there for the patch::

	sha256sums=('3b321a6466d2021f60ed8d4e33bba21db2f23efc2ddd2d9fb775393d9afdfd4d'
            'SKIP'
            'a509731dffd6942c5b31f92b245cdc543965cfe57aeda0854a470c38e608a4ce'
            '3b5cfc9ca9cf778ea2c4b619b933cda26519969df2d764b5a687f63cf59974cd'
            'c175fbb141c3cec013c799f694d88310375ac5456042f6a4a1adc7667836d786'
            '8357f000b2b622e73dcfd41c2bad42b5e99fffe8f7ee64f774aa771f86cef43c'
            '5c1ee81fdd5818442af6081de987f9c1a9ce3c8d183566b3dfc19a8433aa3dde'
            '067e8995fcd6f6ed25e0253e9374c0e179a000c154da3e59ce62634945ac5be9'
	    '54072f5933f8cc556ba032cae67da90cddba1b3bfed13bb4c04f762cbfea55eb')

      Save your changes. Now you can start the kernel build process::

	makepkg -sricfC

