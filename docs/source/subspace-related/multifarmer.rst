.. Subspace AltDocs (c) 2023, Seryoga Leshii

   Subspace AltDocs is licensed under a
   Creative Commons Attribution-ShareAlike 4.0 International License.

   You should have received a copy of the license along with this
   work. If not, see <https://creativecommons.org/licenses/by-sa/4.0/>.


.. _multifarmer-configuration:

***************************
Multi-farmer configuration
***************************

This section is about setting up a multi-farm configuration.
It means runnig one node and connecting farmers hosted on other servers to it.
This guide doesn't make sense if all your servers are on the local network.

This tutorial uses SSH for secure port forwarding.
Therefore, it is highly recommended to secure SSHD.

==================
Node server setup
==================

Create user ``subspace`` if you haven't already done so::

  sudo useradd -m -p ! -s /sbin/nologin -c "" subspace

Also, create a forwarding-only account::

  sudo useradd -m -p ! -s /sbin/nologin -c "" subspace-forwarding

Open the ``/etc/ssh/sshd_config`` file.

If you have limited the users allowed to log in, then add it to this list.
Look for the ``AllowUsers`` setting.
For example::

  AllowUsers user subspace-forwarding

At the end of the file add the following::

  Match User subspace-forwarding
          AllowTcpForwarding yes
          X11Forwarding no
          AllowAgentForwarding no
          PermitTTY no
          GatewayPorts no
          PermitOpen 127.0.0.1:9944
          ForceCommand echo "This user only for port forwarding"
  
Open the file ``/etc/systemd/system/subspace-node.service``
and fill it with the following content::

  [Unit]
  Description=Subspace Node
  After=network.target

  [Service]
  Type=simple
  User=subspace
  ExecStart=/home/subspace/.local/bin/subspace-node \
    --chain <chain> \
    --execution wasm \
    --base-path /home/subspace/.local/share/subspace-node \
    --state-pruning archive \
    --ws-port 9944 \
    --name <name> \
    --validator
  KillSignal=SIGINT
  LimitNOFILE=10000
  Restart=on-failure
  RestartSec=10
  Nice=-5

  [Install]
  WantedBy=multi-user.target

Replace ``<chain>`` with actual chain
and ``<name>`` with desired node moniker.
You can change the binary path and base path.
Also you can run a node in any other convenient way.
  
Switch user::
  
  sudo su -s /bin/bash subspace

Download binaries of actual release or build them by yourself.

Exit from user ``subspace``::

  exit

Switch user::

  sudo su -s /bin/bash subspace-forwarding
  
Create ``~/.ssh`` directory::

  mkdir ~/.ssh

Next, move to farmer servers.


=====================
Farmer servers setup
=====================

Create ``subspace`` account::

  sudo useradd -m -p ! -s /sbin/bash -c "" subspace

Open the ``/etc/systemd/system/subspace-farmer.service`` file::

  Description=Subspace Farmer
  Wants=network.target
  After=network.target
  Wants=subspace-forwarding.service
  After=subspace-forwarding.service

  [Service]
  User=subspace
  ExecStart=/home/subspace/.local/bin/subspace-farmer \
    --base-path /home/subspace/.local/share/subspace-farm \
    --farm path=/home/subspace/.local/share/subspace-farm,size=1000G \
    farm \
    --reward-address <reward-address>
  KillSignal=SIGINT
  LimitNOFILE=25000
  Restart=on-failure
  RestartSec=10
  Nice=-5

  [Install]
  WantedBy=multi-user.target

Replace ``<reward-address>`` with your wallet address.
You can change the binary path, base path and plot size.
Also you can run a farmer in any other convenient way.

Open the ``/etc/systemd/system/subspace-forwarding.service`` file::

  [Unit]
  Description=Subspace Node Forwarding
  After=network.target
  After=subspace-node@%i.service

  [Service]
  Type=simple
  User=subspace
  ExecStart=/usr/bin/ssh -N -o BatchMode=yes \
      -o StrictHostKeyChecking=accept-new \
      -R localhost:9944:localhost:9944 \
      subspace-forwarding@<node-server-ip> \
      -p <node-server-port>
  Restart=always
  RestartSec=10

  [Install]
  WantedBy=multi-user.target

Replace ``<node-server-ip>`` with IP-address of node server
and ``<node-server-port>`` with it's SSH port.

Switch user::

  sudo su -s /bin/bash subspace

Obtain farmer binaries or build them by yourself.

Generate SSH key pair::

  ssh-keygen -t rsa -b 4096 -N "" -f ~/.ssh/id_rsa

Find out public key::

  cat ~/.ssh/id_rsa.pub

Save this key somewhere, you will need it later.

Exit from current user::

  exit


============
Final setup
============

Now, on the node server,
make sure that you are at ``subspace-forwarding`` account.
Then open the file ``~/.ssh/authorized_keys``
and add saved public keys from farmer servers to it (one by line).

Also, start and enable subspace node::

  sudo systemctl enable --now subspace-node

You can check logs with the following command::

  sudo journalctl -f -o cat -u subspace-node


On farmer servers enable forwarding and farmer services::

  sudo systemctl enable --now subspace-forwarding subspace-farmer

You can check forwarder logs via::

  sudo journalctl -f -o cat -u subspace-forwarding

The same for farmer::

  sudo journalctl -f -o cat -u subspace-farmer

Ready!
Note, this configuration is not yet well tested,
so not everything may work as intended.
